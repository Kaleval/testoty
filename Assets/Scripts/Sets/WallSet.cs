using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Wall_set", menuName = "Sets/Walls", order = 0)]
public class WallSet : RuntimeSet<Wall>
{
}
