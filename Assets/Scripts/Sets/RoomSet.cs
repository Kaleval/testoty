using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Room_set", menuName = "Sets/Rooms", order = 0)]
public class RoomSet : RuntimeSet<Room>
{
}
