using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CompetitorSet", menuName = "Sets/Competitors", order = 0)]
public class CompetitorSet : RuntimeSet<Competitor>
{
}
