using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "POI_set", menuName = "Sets/PointsOfInterest", order = 0)]
public class PointOfInterestSet : RuntimeSet<PointOfInterest>
{
}
