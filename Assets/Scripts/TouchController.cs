using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public Vector2Reference playerMovement;
    Vector2 startPos;
    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                startPos = touch.position;
            }
            Vector2 dir = touch.position - startPos;
            playerMovement.value = dir.normalized;
        }
    }
}
