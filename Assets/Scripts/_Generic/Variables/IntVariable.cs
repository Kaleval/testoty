using UnityEngine;

[CreateAssetMenu(fileName = "IntVariable", menuName = "Variables/Int", order = 0)]
public class IntVariable : ScriptableObject
{
    public int value;
}