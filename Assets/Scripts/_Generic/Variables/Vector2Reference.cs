using System;
using UnityEngine;

[Serializable]
public class Vector2Reference
{
    public bool useConstant = true;
    public Vector2 constantValue;
    public Vector2Variable variable;
    // Start is called before the first frame update
    public Vector2 value
    {
        get { return useConstant ? constantValue : variable.value; }
        set
        {
            if (useConstant)
            {
                constantValue = value;
            }
            else
            {
                variable.value = value;
            }
        }
    }
}
