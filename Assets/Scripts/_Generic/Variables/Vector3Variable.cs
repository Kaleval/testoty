using UnityEngine;

[CreateAssetMenu(fileName = "Vector3Variable", menuName = "Variables/Vector3", order = 0)]
public class Vector3Variable : ScriptableObject
{
    public Vector3 value;
}
