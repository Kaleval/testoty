using UnityEngine;

[CreateAssetMenu(fileName = "Vector2Variable", menuName = "Variables/Vector2", order = 0)]
public class Vector2Variable : ScriptableObject
{
    public Vector2 value;
}
