using UnityEngine;

[CreateAssetMenu(fileName = "FloatVariable", menuName = "Variables/Float", order = 0)]
public class FloatVariable : ScriptableObject
{
    public float value;
}