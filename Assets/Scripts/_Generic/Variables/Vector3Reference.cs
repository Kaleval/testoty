using System;
using UnityEngine;

[Serializable]
public class Vector3Reference
{
    public bool useConstant = true;
    public Vector3 constantValue;
    public Vector3Variable variable;
    // Start is called before the first frame update
    public Vector3 value
    {
        get { return useConstant ? constantValue : variable.value; }
        set
        {
            if (useConstant)
            {
                constantValue = value;
            }
            else
            {
                variable.value = value;
            }
        }
    }
}
