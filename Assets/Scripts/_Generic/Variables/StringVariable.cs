using UnityEngine;

[CreateAssetMenu(fileName = "StringVariable", menuName = "Variables/String", order = 0)]
public class StringVariable : ScriptableObject
{
    public string value;
}