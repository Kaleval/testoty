using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public FloatReference time;
    public FloatReference timer;
    public GameEvent onTimerDone;


    private Coroutine crtTimer;
    public void StartTimer(float value = -1f)
    {
        if (value > 0)
        {
            time.value = value;
        }
        timer.value = time.value;
        StartTimer();
    }
    public void StartTimer()
    {
        if (crtTimer != null)
        {
            StopCoroutine(crtTimer);
        }
        timer.value = time.value;
        crtTimer = StartCoroutine(GameTimer());
    }

    public void AddTime(float additionalTime)
    {
        timer.value += additionalTime;
    }

    public void StopTimer()
    {
        StopAllCoroutines();
    }

    IEnumerator GameTimer()
    {
        while (timer.value > 0)
        {
            timer.value -= Time.deltaTime;
            yield return null;
        }
        timer.value = 0;
        onTimerDone?.Raise();

    }
}
