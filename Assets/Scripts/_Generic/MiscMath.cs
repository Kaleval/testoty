using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MiscMath
{
    public static class MiscTrig
    {
        public static Vector3 DirectionFromAngle(float angle)
        {
            Vector3 dir = new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
            return dir;
        }
    }
}
