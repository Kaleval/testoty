using System.Collections.Generic;
using UnityEngine;

public class RuntimeSet<T> : ScriptableObject
{
    public List<T> items = new List<T>();
    public void Add(T t)
    {
        if (!items.Contains(t))
        {
            items.Add(t);
        }
    }
    public void Add(List<T> list)
    {
        foreach (T t in list)
        {
            if (!items.Contains(t))
            {
                items.Add(t);
            }
        }
    }

    public T GetRandomItem()
    {
        if (items.Count > 0)
        {
            int r = Random.Range(0, items.Count);
            return items[r];
        }
        else
        {
            return default(T);
        }
    }

    public void Remove(T t)
    {
        if (items.Contains(t))
        {
            items.Remove(t);
        }
    }
    public void Remove(List<T> list)
    {
        foreach (T t in list)
        {
            if (items.Contains(t))
            {
                items.Remove(t);
            }
        }
    }
    public void Clear()
    {
        items.Clear();
    }
}