using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "Event/GameEvent", order = 0)]
public class GameEvent : ScriptableObject
{
    private readonly List<GameEventResponders.EventResponder> responders =
        new List<GameEventResponders.EventResponder>();
    private readonly List<GameEventListener> listeners =
        new List<GameEventListener>();


    public void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            if (listeners[i] == null)
            {
                listeners.RemoveAt(i);
            }
            else
            {
                listeners[i].OnEventRaised();
            }
        }
        for (int i = responders.Count - 1; i >= 0; i--)
        {
            responders[i].OnEventRaised();

        }

    }


    public void RegisterResponder(GameEventResponders.EventResponder responder)
    {
        if (!responders.Contains(responder))
        {
            responders.Add(responder);
        }
    }

    public void UnregisterResponder(GameEventResponders.EventResponder responder)
    {
        if (responders.Contains(responder))
        {
            responders.Remove(responder);
        }
    }

    public void RegisterListener(GameEventListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
    }

    public void UnregisterListener(GameEventListener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
    }
}