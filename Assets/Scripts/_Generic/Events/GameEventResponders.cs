using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventResponders : MonoBehaviour
{
    public List<EventResponder> eventResponders;
    //public GameEvent gameEvent;
    // public UnityEvent response;
    private void OnEnable()
    {
        foreach (EventResponder er in eventResponders)
        {
            er.Register();
        }
    }
    private void OnDisable()
    {
        foreach (EventResponder er in eventResponders)
        {
            er.Unregister();
        }
    }

    [System.Serializable]
    public class EventResponder
    {
        public string name;
        public GameEvent gameEvent;
        public UnityEvent response;
        internal void OnEventRaised()
        {
            response?.Invoke();
        }

        public void Register()
        {
            gameEvent.RegisterResponder(this);
        }

        public void Unregister()
        {
            gameEvent.UnregisterResponder(this);
        }
    }
}
