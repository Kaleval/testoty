using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : MonoBehaviour
{
    public PointOfInterestSet addToSet;

    void OnEnable()
    {
        addToSet.Add(this);
    }
    private void OnDisable()
    {
        addToSet.Remove(this);
    }
    public Vector3 GetPosition()
    {
        return transform.position;
    }
}
