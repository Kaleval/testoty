using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Room : MonoBehaviour
{
    public WallSet sideWallSet;
    public WallSet wallSet;

    public Wall.WallPosition pos_walls;


    public void GenerateWalls(int positionMask, bool sidewall = false)
    {
        WallSet set = sidewall ? sideWallSet : wallSet;
        if ((positionMask & (int)Wall.WALL_LOCATION.N) != 0)
        {
            Wall w = set.GetRandomItem();
            Instantiate(w, pos_walls.northWall.position, pos_walls.northWall.rotation, pos_walls.northWall);
        }
        if ((positionMask & (int)Wall.WALL_LOCATION.E) != 0)
        {
            Wall w = set.GetRandomItem();
            Instantiate(w, pos_walls.eastWall.position, pos_walls.eastWall.rotation, pos_walls.eastWall);
        }
        if ((positionMask & (int)Wall.WALL_LOCATION.S) != 0)
        {
            Wall w = set.GetRandomItem();
            Instantiate(w, pos_walls.southWall.position, pos_walls.southWall.rotation, pos_walls.southWall);
        }
        if ((positionMask & (int)Wall.WALL_LOCATION.W) != 0)
        {
            Wall w = set.GetRandomItem();
            Instantiate(w, pos_walls.westWall.position, pos_walls.westWall.rotation, pos_walls.westWall);
        }

    }

    public void GenerateWalls()
    {
        foreach (Transform w in pos_walls.GetList())
        {
            GenerateRandomWall(w);
        }
    }

    void GenerateRandomWall(Transform pos)
    {
        Wall w = wallSet.GetRandomItem();
        Instantiate(w, pos.position, pos.rotation, transform);
    }

}
