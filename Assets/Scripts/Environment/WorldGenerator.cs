using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshSurface))]
public class WorldGenerator : MonoBehaviour
{
    public GameEvent OnLevelGenerated;
    NavMeshSurface surface;
    public Vector2Int worldSize = new Vector2Int(3, 3);
    public Vector2 roomSize = new Vector2(5, 5);
    public Room prefStartRoom;
    public RoomSet roomSet;

    public List<Room> rooms;

    int currentLevel = 1;

    public bool delayedRespawn = false;
    public bool delayedNextLevel = false;

    private void Start()
    {
        surface = GetComponent<NavMeshSurface>();
        rooms = new List<Room>();
        GenerateWorld();
    }

    public void DelayedCommand()
    {
        if (delayedNextLevel)
        {
            delayedNextLevel = false;
            NextLevel();
        }
        if (delayedRespawn)
        {
            delayedRespawn = false;
            OnLevelGenerated?.Raise();
        }
    }

    public void DelayNextLevel()
    {
        delayedNextLevel = true;
    }
    public void DelayREspawn()
    {
        delayedRespawn = true;
    }

    void ClearRooms()
    {
        if (rooms.Count > 0)
        {
            for (int i = rooms.Count - 1; i >= 0; i--)
            {
                Room r = rooms[i];
                rooms.Remove(r);
                Destroy(r.gameObject);
            }
        }
    }

    public void NextLevel()
    {
        currentLevel++;
        GenerateWorld(currentLevel);
    }
    public void GenerateWorld(int levelIndex = 1)
    {
        ClearRooms();
        Random.InitState(levelIndex);
        int roomCount = worldSize.x * worldSize.y;
        int startRoomIndex = worldSize.x * Mathf.RoundToInt(worldSize.y / 2) + Mathf.RoundToInt(worldSize.x / 2);
        for (int x = 0; x < worldSize.x; x++)
        {
            for (int y = 0; y < worldSize.y; y++)
            {
                int i = x + y * worldSize.x;
                Vector3 pos = new Vector3();
                pos.x = (-worldSize.x / 2 + x) * roomSize.x;
                pos.z = (-worldSize.y / 2 + y) * roomSize.y;
                Room r;
                if (i == startRoomIndex)
                {
                    r = prefStartRoom;
                }
                else
                {
                    r = roomSet.GetRandomItem();
                }
                Room room = Instantiate(r, pos, transform.rotation, transform);
                rooms.Add(room);
                int sidewallMask = GetSideWallMask(x, y);
                int wallMask = GetWallMask(x, y);
                room.GenerateWalls(sidewallMask, true);
                room.GenerateWalls(wallMask);

            }
        }
        StartCoroutine(DelayedRebuild());
    }

    IEnumerator DelayedRebuild()
    {
        yield return null;
        surface.RemoveData();
        surface.BuildNavMesh();
        OnLevelGenerated?.Raise();
    }

    int GetSideWallMask(int posX, int posY)
    {
        int mask = 0;
        if (posX == 0)
            mask |= (int)Wall.WALL_LOCATION.W;
        if (posX == worldSize.x - 1)
            mask |= (int)Wall.WALL_LOCATION.E;
        if (posY == 0)
            mask |= (int)Wall.WALL_LOCATION.S;
        if (posY == worldSize.y - 1)
            mask |= (int)Wall.WALL_LOCATION.N;

        return mask;
    }

    int GetWallMask(int posX, int posY)
    {
        int mask = 0;
        if (posX != worldSize.x - 1)
            mask |= (int)Wall.WALL_LOCATION.E;
        if (posY != worldSize.y - 1)
            mask |= (int)Wall.WALL_LOCATION.N;

        return mask;
    }
}
