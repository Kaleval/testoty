using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public enum WALL_LOCATION { N = 1, E = 2, S = 4, W = 8 }

    [System.Serializable]
    public struct WallPosition
    {
        public Transform northWall;
        public Transform eastWall;
        public Transform southWall;
        public Transform westWall;

        public List<Transform> GetList()
        {
            List<Transform> list = new List<Transform>();
            list.Add(northWall);
            list.Add(eastWall);
            list.Add(southWall);
            list.Add(westWall);
            return list;
        }
    }
}
