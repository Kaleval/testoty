using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AvatarController : MonoBehaviour
{
    public Competitor competitor;
    public new SkinnedMeshRenderer renderer;
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        competitor.OnCaught += Caught;
        competitor.OnCounting += Count;
        competitor.OnHidden += Hide;
    }

    private void Hide(bool state)
    {
        renderer.enabled = !state;
    }

    private void Caught(bool caught)
    {
        Hide(false);
        animator.SetBool("Wave", caught);
    }

    private void Count(bool caught)
    {
        animator.SetBool("Count", caught);
    }

    void Update()
    {
        UpdateAnimation();
    }

    void UpdateAnimation()
    {
        if (competitor)
        {
            float speed = competitor.movementController.GetSpeed();
            animator.SetFloat("MoveSpeed", speed);
        }
    }
}
