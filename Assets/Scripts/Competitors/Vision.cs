using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour
{
    public float viewRadius = 5;
    [Range(0, 360)]
    public float viewAngle;
    public int fovStep = 10;

    public bool trackTargets = false;
    public LayerMask targetMask;
    public LayerMask obstacleMask;
    public GameObject visionMesh;


    public List<Transform> visibleTargets = new List<Transform>();

    public float targetsInRadius;

    public delegate void targetFound(Competitor target);
    public targetFound OnTargetFound;

    public bool hideTargetOutsideFOV = false;
    private void Start()
    {
        if (trackTargets)
        {
            StartTrackingTargets();
        }
    }

    public void StartTrackingTargets()
    {
        trackTargets = true;
        StartCoroutine(TrackTargets(0.2f));
    }

    public void StopTrackingTargets()
    {
        trackTargets = false;
    }

    public void ShowFOV(bool show = true)
    {
        visionMesh.SetActive(show);
    }

    void FindVisibleTargets()
    {
        List<Transform> prevTargets = new List<Transform>(visibleTargets);
        visibleTargets.Clear();
        Collider[] targetInRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
        targetsInRadius = targetInRadius.Length;
        for (int i = 0; i < targetInRadius.Length; i++)
        {
            Transform target = targetInRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float distToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position + Vector3.up, dirToTarget, distToTarget, obstacleMask))
                {
                    if (target.GetComponent<Competitor>() is Competitor targetComp)
                    {
                        if (targetComp.gameObject != gameObject)
                        {
                            if (!visibleTargets.Contains(target))
                            {
                                visibleTargets.Add(target);
                            }
                            if (prevTargets.Contains(target))
                            {
                                prevTargets.Remove(target);
                            }
                            else
                            {
                                // new target
                                OnTargetFound?.Invoke(target.GetComponent<Competitor>());
                                if (hideTargetOutsideFOV)
                                {
                                    target.GetComponent<Competitor>().Hide(false);
                                }
                            }
                        }

                    }
                }
            }
        }
        if (hideTargetOutsideFOV)
        {
            for (int i = 0; i < prevTargets.Count; i++)
            {
                Transform target = prevTargets[i];
                if (target.GetComponent<Competitor>() is Competitor targetComp)
                {
                    if (targetComp.gameObject != gameObject)
                    {
                        targetComp.Hide(true);
                    }
                }
            }
        }
    }

    IEnumerator TrackTargets(float delay)
    {
        while (trackTargets)
        {
            yield return new WaitForSeconds(delay);
            FindVisibleTargets();
        }
        visibleTargets.Clear();
    }

    void DrawFOV()
    {
        if (fovStep == 0)
        {
            return;
        }
        int fovLines = (int)(viewAngle / fovStep);
        for (int i = 0; i < fovLines; i++)
        {
            // Debug.DrawLine(transform.position,)
        }
    }


}
