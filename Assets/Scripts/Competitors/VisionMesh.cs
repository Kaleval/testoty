using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiscMath;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class VisionMesh : MonoBehaviour
{
    public Vision vision;
    // Start is called before the first frame update
    new MeshRenderer renderer;
    MeshFilter filter;
    Mesh viewMesh;

    private void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        filter = GetComponent<MeshFilter>();
        viewMesh = new Mesh();
        filter.mesh = viewMesh;
    }
    void LateUpdate()
    {
        DrawMesh();
    }
    void DrawMesh()
    {
        float viewAngle = vision.viewAngle;
        int stepCount = vision.fovStep;
        float stepAngle = viewAngle / stepCount;
        List<Vector3> viewPoints = new List<Vector3>();
        for (int i = 0; i <= stepCount; i++)
        {
            float angle = vision.transform.eulerAngles.y - viewAngle / 2 + stepAngle * i;
            //Debug.DrawLine(transform.position, transform.position + MiscTrig.DirectionFromAngle(angle) * vision.viewRadius, Color.red);
            ViewCastInfo newViewcast = ViewCast(angle);
            Vector3 point = newViewcast.point;
            point.y = 0;
            viewPoints.Add(point);
        }
        int vertCount = viewPoints.Count + 1;
        Vector3[] vertices = new Vector3[vertCount];
        int[] tris = new int[(vertCount - 2) * 3];

        vertices[0] = Vector3.zero;
        for (int i = 0; i < vertCount - 1; i++)
        {
            vertices[i + 1] = transform.InverseTransformPoint(viewPoints[i]);
            if (i < vertCount - 2)
            {
                tris[i * 3] = 0;
                tris[i * 3 + 1] = i + 1;
                tris[i * 3 + 2] = i + 2;
            }
        }
        viewMesh.Clear();
        viewMesh.vertices = vertices;
        viewMesh.triangles = tris;
        viewMesh.RecalculateNormals();
    }

    ViewCastInfo ViewCast(float globalAngle)
    {
        Vector3 dir = MiscTrig.DirectionFromAngle(globalAngle);
        if (Physics.Raycast(transform.position + Vector3.up, dir, out RaycastHit hit, vision.viewRadius, vision.obstacleMask))
        {
            return new ViewCastInfo(true, hit.point, hit.distance, globalAngle);
        }
        return new ViewCastInfo(false, transform.position + dir * vision.viewRadius, vision.viewRadius, globalAngle);
    }

    public struct ViewCastInfo
    {
        public bool hit;
        public Vector3 point;
        public float distance;
        public float angle;
        public ViewCastInfo(bool hit, Vector3 point, float distance, float angle)
        {
            this.hit = hit;
            this.point = point;
            this.distance = distance;
            this.angle = angle;
        }
    }
}
