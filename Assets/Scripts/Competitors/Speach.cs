using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Speach : MonoBehaviour
{
    public CanvasGroup bubbleCanvas;
    public TextMeshProUGUI bubbleText;

    private void Update()
    {
        Vector3 lookPos = -Camera.main.transform.forward;
        //lookPos.y = transform.position.y;
        transform.LookAt(transform.position + lookPos, Vector3.up);
    }
    public void Say(string text)
    {
        bubbleCanvas.alpha = 1;
        StopAllCoroutines();
        bubbleText.text = text;
        float t = Mathf.Max(text.Length * 0.1f, 1.5f);
        StartCoroutine(HideText(t));
    }

    IEnumerator HideText(float time)
    {
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return null;
        }
        float hidetime = 2;
        float alpha = bubbleCanvas.alpha;
        while (alpha > 0)
        {
            alpha -= Time.deltaTime / hidetime;
            bubbleCanvas.alpha = alpha;
        }

    }
}
