using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovementController : MovementController
{
    public Vector2Reference moveDirection;
    protected CharacterController charController;

    void Start()
    {
        charController = GetComponent<CharacterController>();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.rigidbody is Rigidbody rb)
        {
            Vector3 dir = charController.velocity.normalized;
            float force = 1;

            rb.AddForceAtPosition(dir * force, hit.point, ForceMode.Impulse);
            Debug.Log("Hit");
        }
    }

    public override void UpdateMovement()
    {
        base.UpdateMovement();
        float x = moveDirection.value.x;
        float y = moveDirection.value.y;
        if ((x == 0 & y == 0) || !allowMovement)
        {
            charController.SimpleMove(Vector3.zero);
            return;
        }

        Vector3 direction = (new Vector3(x, 0, y)).normalized;
        Quaternion rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), Time.deltaTime * rotateSpeed.value);
        transform.rotation = rotation;
        Vector3 velocity = direction * maxSpeed.value;
        charController.SimpleMove(velocity);
    }

    public override float GetSpeed()
    {
        return charController.velocity.magnitude;
    }
}
