using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NavAgentMovementController : MovementController
{
    public PointOfInterestSet hidePoints;
    protected NavMeshAgent navMeshAgent;
    PointOfInterest target;
    Competitor competitor;
    bool hasTarget = false;

    bool isCoolingDown = false;



    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        competitor = GetComponent<Competitor>();
    }

    void Start()
    {
        competitor.vision.OnTargetFound += OnTargetFound;
    }

    private void OnTargetFound(Competitor target)
    {
        if (!competitor.isSeeker)
        {
            if (target.isSeeker)
            {
                hasTarget = false;
            }
            else
            {
                if (target.isCaught)
                {
                    navMeshAgent.SetDestination(target.transform.position);
                }
            }
        }
    }

    public override void AllowMovement(bool allow = true)
    {
        hasTarget = false;
        isCoolingDown = false;
        StopAllCoroutines();
        base.AllowMovement(allow);
        navMeshAgent.isStopped = !allow;
    }
    public override void UpdateMovement()
    {
        if (!allowMovement)
        {
            return;
        }
        if (hasTarget)
        {
            if (GetDistanceToTarget() < 1)
            {
                hasTarget = false;
                StartCoroutine(Cooldown());
            }
            return;
        }
        if (isCoolingDown)
        {
            return;
        }
        target = GetNewTarget();
        if (!target)
        {
            return;
        }
        navMeshAgent.SetDestination(target.GetPosition());
        hasTarget = true;
    }

    PointOfInterest GetNewTarget()
    {
        PointOfInterest p = hidePoints.GetRandomItem();
        return p;
    }


    IEnumerator Cooldown()
    {
        isCoolingDown = true;

        float min = competitor.isSeeker ? 0.2f : 1f;
        float max = competitor.isSeeker ? 1f : 5f;
        float t = Random.Range(min, max);
        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }
        isCoolingDown = false;
    }


    float GetDistanceToTarget()
    {
        float dist = (transform.position - target.GetPosition()).magnitude;
        return dist;
    }

    public override float GetSpeed()
    {
        return navMeshAgent.velocity.magnitude;
    }
}
