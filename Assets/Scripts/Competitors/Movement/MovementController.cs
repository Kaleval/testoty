using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public FloatReference maxSpeed;
    public FloatReference rotateSpeed;


    public bool allowMovement = false;
    protected Vector3 targetDirection;

    public virtual void AllowMovement(bool allow = true)
    {
        allowMovement = allow;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMovement();
    }

    public virtual void UpdateMovement()
    {
    }

    public virtual float GetSpeed()
    {
        return 0;
    }

    public virtual void Seek()
    {
    }
}
