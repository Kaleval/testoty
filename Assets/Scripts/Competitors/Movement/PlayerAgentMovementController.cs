using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerAgentMovementController : MovementController
{
    protected NavMeshAgent navMeshAgent;
    Competitor competitor;
    public Vector2Reference moveDirection;

    Vector3 prevPos;
    float speed;


    void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        competitor = GetComponent<Competitor>();
    }

    void Start()
    {
        prevPos = transform.position;
    }


    public override void UpdateMovement()
    {
        base.UpdateMovement();
        if (!allowMovement)
        {
            speed = 0;
            return;
        }
        float x = moveDirection.value.x;
        float y = moveDirection.value.y;
        if (!(x == 0 & y == 0))
        {
            Vector3 direction = (new Vector3(x, 0, y)).normalized;
            navMeshAgent.Move(direction * Time.deltaTime * maxSpeed.value);
            Quaternion rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction, Vector3.up), Time.deltaTime * rotateSpeed.value);
            transform.rotation = rotation;
        }
        speed = (prevPos - transform.position).magnitude / Time.deltaTime;
        prevPos = transform.position;

    }

    public override float GetSpeed()
    {
        return speed;
    }
}
