using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Competitor : MonoBehaviour
{
    [Header("References")]
    public Vision vision;
    public Speach speach;
    public MovementController movementController;

    public bool isSeeker = false;
    public bool isCaught = true;
    internal CompetitorSet caughtSet;

    public delegate void boolDelegate(bool state);
    public boolDelegate OnCaught;
    public boolDelegate OnCounting;
    public boolDelegate OnHidden;

    private void OnTriggerEnter(Collider other)
    {
        if (!isSeeker && !isCaught)
        {
            if (other.gameObject.tag == "Competitor")
            {
                if (other.gameObject.GetComponent<Competitor>() is Competitor comp)
                {
                    if (comp.isCaught)
                    {
                        comp.SetFree();
                    }
                }
            }
        }
    }
    private void Awake()
    {
        movementController = GetComponent<MovementController>();
    }

    private void OnDestroy()
    {
        vision.OnTargetFound -= OnTargetFound;
    }

    public void AllowMovement(bool allow = true)
    {
        if (allow)
        {
            vision.StartTrackingTargets();
        }
        else
        {
            vision.StopTrackingTargets();
        }
        movementController.AllowMovement(allow);
        vision.OnTargetFound += OnTargetFound;
    }

    public void SetSeeker(bool seeker = true, FloatReference seekTimer = null)
    {
        isSeeker = seeker;
        if (seeker)
        {
            OnCounting?.Invoke(true);
            if (seekTimer != null)
            {
                StartCoroutine(CountLoud(seekTimer));
            }
        }
    }

    IEnumerator CountLoud(FloatReference timer)
    {
        yield return null;
        int lastValue = (int)timer.value + 1;
        while (timer.value > 0)
        {
            if (timer.value < lastValue)
            {
                lastValue = Mathf.FloorToInt(timer.value);
                speach.Say($"{lastValue.ToString()}!");
            }
            yield return null;
        }
    }

    public void SeekTargets(bool seek = true)
    {
        if (seek)
        {
            OnCounting?.Invoke(false);
            if (movementController)
            {
                movementController.Seek();
            }
            speach.Say($"You can run,\nbut you can't hide!");
            vision.ShowFOV(true);
        }
        else
        {
            vision.ShowFOV(false);
        }
    }

    public void OnTargetFound(Competitor target)
    {
        if (isSeeker)
        {
            if (!target.isSeeker && !target.isCaught)
            {
                target.Catch();
            }
        }
    }

    public void hideTargetOutsideFOV(bool hide = true)
    {
        vision.hideTargetOutsideFOV = hide;
    }

    public void Hide(bool hide = true)
    {
        OnHidden?.Invoke(hide);
    }

    public void Catch()
    {
        if (!isCaught)
        {
            isCaught = true;
            AllowMovement(false);
            if (caughtSet)
            {
                caughtSet.Add(this);
            }
            OnCaught?.Invoke(true);
            speach.Say($"Help!");
        }
    }

    public void SetFree()
    {
        if (isCaught)
        {
            isCaught = false;
            if (caughtSet)
            {
                caughtSet.Remove(this);
            }
            AllowMovement(true);
            OnCaught?.Invoke(false);
            speach.Say($"Thanks!");
        }
    }

    public void SetAvatar(AvatarLib.AvatarEntry avatar)
    {
        AvatarController avatarController = Instantiate(avatar.avatarController, transform);
        avatarController.competitor = this;
    }

}
