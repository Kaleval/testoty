using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ImageDimmer : MonoBehaviour
{
    public Image image;
    float targetValue = 1;
    float currentValue = 1;

    float time = 1.5f;

    public GameEvent OnImageHidden;
    public GameEvent OnImageShown;

    public void HideImage()
    {
        StopAllCoroutines();
        targetValue = 0;
        StartCoroutine(Dim(OnImageHidden));
    }

    public void ShowImage()
    {
        StopAllCoroutines();
        targetValue = 1;
        StartCoroutine(Dim(OnImageShown));
    }

    IEnumerator Dim(GameEvent callback)
    {
        Color c = image.color;
        currentValue = c.a;
        while (currentValue != targetValue)
        {
            yield return null;
            currentValue = Mathf.MoveTowards(currentValue, targetValue, time * Time.deltaTime);
            c.a = currentValue;
            image.color = c;
        }
        callback?.Raise();
    }
}
