using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UI_CaughtCounter : MonoBehaviour
{
    [Header("Sets")]
    public CompetitorSet caughtSet;
    public CompetitorSet competitorSet;
    public CompetitorSet seekerSet;

    TextMeshProUGUI label;
    void Start()
    {
        label = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        int cnt_runners = competitorSet.items.Count - seekerSet.items.Count; 
        int cnt_caught = caughtSet.items.Count;
        label.text = $"Caught: {cnt_caught}/{cnt_runners}";
    }
}
