using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UI_time : MonoBehaviour
{
    public FloatReference time;
    public TextMeshProUGUI labelS;
    public TextMeshProUGUI labelMs;

    RuntimeSet<Competitor> competitors;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        TimeSpan timeLeft = TimeSpan.FromSeconds(time.value);

        labelS.text = timeLeft.ToString("%s");
        labelMs.text = timeLeft.ToString("%f");
    }
}
