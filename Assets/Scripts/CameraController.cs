using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public CompetitorSet playerSet;
    public Vector3 offset;
    public float SmoothTime = 0.3f;
    private Vector3 velocity = Vector3.zero;
    Transform target;

    private void Start()
    {
        LookForTarget();
    }
    private void LateUpdate()
    {

        if (target != null)
        {
            // update position
            Vector3 targetPosition = target.position + offset;
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, SmoothTime);

            // update rotation
            transform.LookAt(target);
        }
        else
        {
            LookForTarget();
        }
    }

    public void LookForTarget()
    {
        if (playerSet != null)
        {
            if (playerSet.items.Count > 0)
            {
                target = playerSet.items[0].transform;
            }
        }
    }
}
