using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AvatarLib", menuName = "Libs/AvatarLib", order = 0)]
public class AvatarLib : ScriptableObject
{
    public List<AvatarEntry> avatars;

    [System.Serializable]
    public class AvatarEntry
    {
        public string avatarName;
        public AvatarController avatarController;
    }

    public AvatarEntry GetAvatar(int index = -1)
    {
        if (index >= avatars.Count){
            index = 0;
        }
        if (index < 0)
        {
            int r = Random.Range(0, avatars.Count);
            return avatars[r];
        }
        else
        {
            return avatars[index];
        }
    }
}
