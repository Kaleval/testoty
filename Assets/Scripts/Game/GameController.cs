using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    [Header("Events")]
    public GameEvent onSpawnCompetitors;

    void Start()
    {
        SpawnPlayers();
    }
    void SpawnPlayers()
    {
        onSpawnCompetitors.Raise();
    }

    public void StartGame()
    {
    }

    void EndGame()
    {
    }

}
