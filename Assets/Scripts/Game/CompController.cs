using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompController : MonoBehaviour
{
    [Header("References")]
    public IntReference playerCount;
    public IntReference seekerCount;
    [Header("Sets")]
    public CompetitorSet playersSet;
    public CompetitorSet seekersSet;
    public CompetitorSet competitorSet;
    public CompetitorSet caughtSet;

    public PointOfInterestSet spawnPointSet;

    [Header("Libraries")]
    public AvatarLib avatarLib;

    [Header("Prefabs")]
    public Competitor prefPlayerCompetitor;
    public Competitor prefCompetitor;

    [Header("Events")]
    public GameEvent OnPlayersSpawned;
    public GameEvent OnRolesSet;
    public GameEvent OnAllCaught;
    public GameEvent OnPlayerWon;
    public GameEvent OnPlayerLost;
    public GameEvent OnResults;

    [Header("Timers")]
    public Timer countDownTimer;
    public void SpawnCompetitors()
    {
        ClearPlayers();
        Debug.Log($"Spawn players {spawnPointSet.items.Count}");
        for (int i = 0; i < playerCount.value; i++)
        {
            Vector3 pos;
            if (spawnPointSet)
            {
                if (i < spawnPointSet.items.Count)
                {
                    pos = spawnPointSet.items[i].GetPosition();
                }
                else
                {
                    float x = Random.Range(-5f, 5f);
                    float z = Random.Range(-5f, 5f);
                    pos = new Vector3(x, 0, z);
                }
            }
            else
            {
                float x = Random.Range(-5f, 5f);
                float z = Random.Range(-5f, 5f);
                pos = new Vector3(x, 0, z);
            }
            SpawnCompetitor(pos, i == 0); // first one is player
        }
        OnPlayersSpawned?.Raise();
    }
    private void OnDestroy()
    {
        competitorSet.Clear();
        playersSet.Clear();
        seekersSet.Clear();
        caughtSet.Clear();
    }

    public void SpawnCompetitor(Vector3 position, bool isPlayer = false)
    {
        Competitor competitor = Instantiate(isPlayer ? prefPlayerCompetitor : prefCompetitor, position, transform.rotation);
        competitor.SetAvatar(avatarLib.GetAvatar());
        competitorSet.Add(competitor);
        competitor.caughtSet = caughtSet;
        competitor.transform.LookAt(Vector3.zero);
        competitor.OnCaught += CheckCaughtCount;
        if (isPlayer)
        {
            playersSet.Add(competitor);
        }
    }

    public void DecideWinner()
    {
        if (caughtSet.items.Count == competitorSet.items.Count)
        {
            // seeker wins
            if (playersSet.items[0].isSeeker)
            {
                OnPlayerWon?.Raise();
            }
            else
            {
                OnPlayerLost?.Raise();
            }
        }
        else
        {
            // uncought players win
            if (playersSet.items[0].isCaught)
            {
                OnPlayerLost?.Raise();
            }
            else
            {
                OnPlayerWon?.Raise();
            }
        }
        OnResults?.Raise();
    }

    private void CheckCaughtCount(bool state)
    {
        if (state)
        {
            int cnt_runners = competitorSet.items.Count - seekersSet.items.Count;
            int cnt_caught = caughtSet.items.Count;
            if (cnt_runners == cnt_caught)
            {
                OnAllCaught?.Raise();
            }
        }
    }

    void ClearPlayers()
    {
        for (int i = competitorSet.items.Count - 1; i >= 0; i--)
        {
            Competitor c = competitorSet.items[i];
            Destroy(c.gameObject);
        }
        competitorSet.Clear();
        playersSet.Clear();
        seekersSet.Clear();
        caughtSet.Clear();
    }
    public void ClearRoles()
    {
        for (int i = competitorSet.items.Count - 1; i >= 0; i--)
        {
            Competitor c = competitorSet.items[i];
            c.isSeeker = false;
            c.isCaught = false;
        }
    }


    public void SetPlayerSeeks()
    {
        SetSeekers(playersSet.items);
        // SetRunnersHidden(true);
    }

    public void SetNonPlayerSeeker()
    {
        int r = Random.Range(1, competitorSet.items.Count);
        List<Competitor> seekers = new List<Competitor>();
        seekers.Add(competitorSet.items[r]);
        SetSeekers(seekers);
        // SetRunnersHidden(false);
    }

    public void SetRandomSeeker()
    {
        List<Competitor> seekers = new List<Competitor>();
        seekers.Add(competitorSet.GetRandomItem());
        SetSeekers(seekers);
    }

    void SetSeekers(List<Competitor> seekers)
    {
        caughtSet.Clear();
        // clear old seekers
        foreach (Competitor c in seekersSet.items)
        {
            c.SeekTargets(false);
            c.SetSeeker(false);
            c.AllowMovement(false);
        }
        seekersSet.Clear();
        //set new seekers
        seekersSet.Add(seekers);
        foreach (Competitor c in seekersSet.items)
        {
            c.SeekTargets(false);
            c.AllowMovement(false);
            c.SetSeeker(true, countDownTimer.timer);
        }
        OnRolesSet.Raise();
    }

    void SetRunnersHidden(bool hide = true)
    {
        foreach (Competitor c in seekersSet.items)
        {
            c.hideTargetOutsideFOV(hide);
        }
    }

    public void AllowSeeking(bool allow = true)
    {
        foreach (Competitor c in seekersSet.items)
        {
            c.SeekTargets(allow);
            c.AllowMovement(allow);
        }
    }

    public void AllowRunning(bool allow = true)
    {
        foreach (Competitor c in competitorSet.items)
        {
            if (!c.isSeeker)
            {
                c.AllowMovement(allow);
            }
        }

    }

}
