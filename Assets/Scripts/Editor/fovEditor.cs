using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MiscMath;

[CustomEditor(typeof(Vision))]
public class fovEditor : Editor
{

    void OnSceneGUI()
    {
        Vision vision = (Vision)target;
        Handles.color = Color.white;
        Vector3 viewAngleA = MiscTrig.DirectionFromAngle(-vision.viewAngle / 2 + vision.transform.eulerAngles.y);
        Vector3 viewAngleB = MiscTrig.DirectionFromAngle(vision.viewAngle / 2 + vision.transform.eulerAngles.y);
        Handles.DrawLine(vision.transform.position, vision.transform.position + viewAngleA * vision.viewRadius);
        Handles.DrawLine(vision.transform.position, vision.transform.position + viewAngleB * vision.viewRadius);

        Handles.DrawWireArc(vision.transform.position, Vector3.up, viewAngleA, vision.viewAngle, vision.viewRadius);

        foreach (Transform target in vision.visibleTargets)
        {
            Handles.color = Color.red;
            Handles.DrawLine(vision.transform.position, target.transform.position);
        }
    }
}
